---
layout: job_family_page
title: UX Research Operations Coordinator
---

At GitLab, UX Research Operations Coordinators collaborate with our Product Designers, Product Managers, UX Researchers, and the rest of the community to manage participant recruitment for UX studies. UX Research Operations Coordinators report to the UX Research Director. Unless otherwise specified, all UX Research Operations Coordinator roles at GitLab share the following responsibilities and requirements:

## Responsibilities

- Drive all aspects of the UX research operations coordination program across GitLab
- Maintain deep working knowledge of the participant recruitment process
- Use GitLab for the intake of questions, concerns, and requests related to participant recruitment
- Maintain tight communication with team members to relay status of requests
- Anticipate and troubleshoot recruitment challenges as they arise
- Determine the best possible recruitment source for a given study
- Understand the pros and cons of each recruitment source
- Proactively report out key data
- Use participant panels for recruitment
- Stay aware of and implement GDPR (General Data Protection Regulation) policies
- Develop and manage participant panel growth strategies
- Establish best practices for participant recruitment

## Requirements

* [Self-motivated and self-managing](/handbook/values/#efficiency), with strong organizational skills.
* Share our [values](/handbook/values/), and work in accordance with those values.
* Simultaneously manage multiple projects and time-driven tasks
* Strong communication and collaboration skills to keep teams informed on progress
* Empathetic, curious, and open-minded
* Ability to thrive in a fully remote organization
* Ability to use GitLab

### UX Research Operations Coordinator (Intermediate)

The UX Research Operations Coordinator (Intermediate) reports to the [UX Research Director](/job-families/engineering/ux-research-manager/#director-ux-research).

#### Job Grade

The UX Research Operations Coordinator (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### UX Research Operations Coordinator (Intermediate) Responsibilities

* Manage all aspects of participant recruitment for user experience research studies, including but not limited to: recruiting from multiple sources, outreach, screening, scheduling, participation agreements, privacy, and incentives management.
* Identify team needs and gaps. Continuously examine, experiment, and measure improvements to our research operations and processes.
* Grow, foster, and administer new participant panels, along with our existing panel, [GitLab First Look](https://about.gitlab.com/community/gitlab-first-look/).
* Maintain UX research operations handbook documentation with accurate, standardized, and transparent processes and procedures.
* Use our databases to filter and screen participants.
* Create and manage libraries of screeners for team members to use.
* Collaborate with Product Designers, Product Managers, and UX Researchers to effectively understand their research recruitment needs and to drive their requests to successful completion.
* Proactively and routinely use social media to showcase UX Research insights and solutions to the wider GitLab community.
* Manage relationships with third-party providers, such as recruitment platforms, digital rewards solutions, etc.
* Respond to inquiries from research participants.
* Monitor progress of research operations by creating, maintaining, and communicating monthly reports on key data points such as: budget, number of participants by research study type, forecasting, and other metrics.
* Administration of our research toolsets:  Qualtrics, UserTesting.com, Dovetail, Respondent, Rybbon, etc.

#### UX Research Operations Coordinator (Intermediate) Requirements

* Some SQL query skills and willing to grow those skills.
* Exquisite organizational skills: regularly managing multiple research projects at a time.
* Experience in UX Research Operations Coordination or a related role that involves administration and coordination.
* In-depth experience with scheduling sessions and communicating with internal and external participants/stakeholders. 
* Excellent written and verbal communication skills.
* Experience with using a ticket-based system to track work requests.
* Able to use GitLab for communication and work management

### Senior UX Research Operations Coordinator

The Senior UX Research Operations Coordinator reports to the [UX Research Director](/job-families/engineering/ux-research-manager/#director-ux-research).

#### Senior UX Research Operations Coordinator Job Grade

The UX Research Operations Coordinator is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior UX Research Operations Coordinator Responsibilities

* Extends the UX Research Operations Coordinator (Intermediate) responsibilities.
* Proactively identify, build, and streamline processes that result in efficient research operations.
* Proactively solicit requirements and feedback from Product Designers, Product Managers, and UX Researchers to further optimize research operations.
* Act as the strategic owner of our research participant panels, advancing their overall health, growth, and usage within GitLab.
* Establish, drive, and/or identify social events to grow our participant panel.
* Be the social media ambassador for UX Research at GitLab. Maintain social media accounts that promote our research efforts and aid participant recruitment.
* Formalize the research operations coordination effort into a program of work with extensive processes and documentation to improve visibility and increase efficiency.
* Define and deliver on a UX research operations roadmap that improves efficiency and addresses gaps.
* Evangelize the value of UX research along with completed research efforts and insights across GitLab and to our customers.

#### Senior UX Research Operations Coordinator Requirements

* Extends the UX Research Operations Coordinator (Intermediate) requirements.
* Strong SQL query skills.
* Experience in UX Research Operations Coordination or a related role that involves administration and coordination.

## Performance Indicators

* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)
* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Selected candidates will be invited to schedule a 30-minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters. In this call, we will discuss your experience, understand what you are looking for in a UX Research Operations Coordinator role, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
* Next, candidates will be scheduled for a 45-minute interview with a UX Researcher. 
* Finally, candidates will be invited to a 1-hour interview with our Director of UX Research.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
